import React from 'react';
import TodoItem from '../todo-item/todo-item';

const TodoList = ({data, doneData, deleteData, filterStatus, checkItemToggle, markImp, deleteItem, returnFromDelete, deletePermanently}) =>{
  if(data){
    if(filterStatus === 1){
      const items = data.map( (item) => {
        return <TodoItem key={item.txt} id={item.id} txt={item.txt} done={item.done} 
        important={item.important} checkItemToggle={checkItemToggle} markImp={markImp} deleteItem={deleteItem} />
      })
      return(
        <div>
          {items}
        </div>
      )
    }
    else if(filterStatus === 2){
      const items = doneData.map((item) => {
        return <TodoItem key={item.txt} id={item.id} txt={item.txt} done={item.done} 
        important={item.important} checkItemToggle={checkItemToggle} markImp={markImp} deleteItem={deleteItem} />
      })
      return (
        <div>
          {items}
        </div>
      )
    }
    else if(filterStatus === 3){
      const items = deleteData.map((item) => {
        return <TodoItem key={item.txt} id={item.id} txt={item.txt} done={item.done} 
        important={item.important} checkItemToggle={checkItemToggle} markImp={markImp} deleteItem={deleteItem} 
        returnFromDelete={returnFromDelete} deletePermanently={deletePermanently} />
      })
      return (
        <div>
          {items}
        </div>
      )
    }
  }
}

export default TodoList;