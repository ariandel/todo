import React from 'react';
import moduleStyles from './todo-item.module.scss';
const FA = require('react-fontawesome');

const TodoItem = ({id,txt,done,important, checkItemToggle, deleteItem, markImp, returnFromDelete, deletePermanently}) =>{

  const styles = {
    color: done ? '#FFC42C' : '#222',
    fontWeight: important ? '700' : '400'
  }
  const checkStatus = done ? 'checked' : '';

  if(returnFromDelete){
    return(
      <div className={moduleStyles.item}>
        <label htmlFor={id} className={moduleStyles.input_wrp}>
          <input id={id} type="checkbox" checked={checkStatus} onChange={() => checkItemToggle(id, done)} />
          <div className={moduleStyles.input_wrp__custom_box}></div>
          <p style={styles} className={moduleStyles.input_wrp__txt}>{txt}</p>
        </label>
        <div className={moduleStyles.buttons_wrp}>
          <div className={`${moduleStyles.basic_icon} ${moduleStyles.return_icon}`} onClick={ () => returnFromDelete(id) }><FA name='reply-all' /></div>
          <div className={`${moduleStyles.basic_icon} ${moduleStyles.delete_icon}`} onClick={ () => deletePermanently(id) }><FA name='minus-circle' /></div>
        </div>
      </div>
    ) 
  }
  else{
    return(
      <div className={moduleStyles.item}>
        <label htmlFor={id} className={moduleStyles.input_wrp}>
          <input id={id} type="checkbox" checked={checkStatus} onChange={() => checkItemToggle(id, done)} />
          <div className={moduleStyles.input_wrp__custom_box}></div>
          <p style={styles} className={moduleStyles.input_wrp__txt}>{txt}</p>
        </label>
          <div className={moduleStyles.buttons_wrp}>
            <div className={`${moduleStyles.basic_icon} ${moduleStyles.imp_icon}`} onClick={ () => markImp(id, important) }><FA name="exclamation-triangle" /></div>
            <div className={`${moduleStyles.basic_icon} ${moduleStyles.delete_icon}`} onClick={ () => deleteItem(id) }><FA name='times-circle' /></div>
          </div>
      </div>
    )
  }
}

export default TodoItem;