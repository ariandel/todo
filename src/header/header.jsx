import React from 'react';
import moduleStyle from './header.module.scss';
import HeaderItem from './header-item/header-item';
import Logo from './../logo/logo';

export default class Header extends React.Component{
  state = [
      {txt: 'All', active: true},
      {txt: 'Done', active: false},
      {txt: 'Deleted', active: false}
  ]

  filterStatusUpd = ({txt}, elem) =>{
    this.setState( (state)  =>{
      for(let i = 0; i < this.state.length; i++){
        const currentElem = this.state[i];
        currentElem.active = false;
        if(this.state[i].txt === txt){
          currentElem.active = !currentElem.active;
        }
      }
    });
  }

  accentBlockWidthCount = (elem) =>{
    if(elem){
      this.accentBlockStyle = {width: elem.target.offsetWidth}
    }
  }
  
  accentBlockStyle = {
    width: '33px'
  }

  
  render(){

    const headerItems = this.state.map( (elem) =>{
      return(
        <HeaderItem key={elem.txt}
        txt={elem.txt} active={elem.active} filterAll={this.props.filterAll} 
        filterDone={this.props.filterDone} filterDeleted={this.props.filterDeleted} 
        filterStatusUpd={this.filterStatusUpd} accentBlockWidthCount={this.accentBlockWidthCount} />
      )
    });
    
    return(
      <header className={moduleStyle.header}>
        <div className={moduleStyle.header__logo}>
          <Logo />
        </div>
        <nav className={moduleStyle.header__nav}>
          <ul className={moduleStyle.header__nav__items}>
            {headerItems}
            <div style={this.accentBlockStyle} className={moduleStyle.accent_block}></div>
          </ul>
        </nav>
        <div>
  
        </div>
      </header>
    )
  }
}