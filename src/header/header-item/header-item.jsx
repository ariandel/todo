import React from 'react';
import moduleStyles from '../header.module.scss';

const HeaderItem = ({txt,active, filterAll, filterDone, filterDeleted, filterStatusUpd, activeFilterStyle, accentBlockWidthCount}) =>{
  if(txt === 'All'){
    return(
      <li 
        style={activeFilterStyle} 
        className={`${moduleStyles.header__nav__items__item} ${active ? moduleStyles.active : ''}`} 
        onClick={ (elem) =>{filterAll(); filterStatusUpd({txt}, elem); accentBlockWidthCount(elem);}}>
        {txt}
      </li>
    )
  }
  else if(txt === 'Done'){
    return(
      <li 
        style={activeFilterStyle} 
        className={`${moduleStyles.header__nav__items__item} ${active ? moduleStyles.active : ''}`} 
        onClick={ (elem) =>{filterDone(); filterStatusUpd({txt}, elem); accentBlockWidthCount(elem);}}>
        {txt}
      </li>
    )
  }
  else if(txt === 'Deleted'){
    return(
      <li 
        style={activeFilterStyle} 
        className={`${moduleStyles.header__nav__items__item} ${active ? moduleStyles.active : ''}`} 
        onClick={ (elem) =>{filterDeleted(); filterStatusUpd({txt}, elem); accentBlockWidthCount(elem);}}>
        {txt}
      </li>
    )
  }
}

export default HeaderItem;