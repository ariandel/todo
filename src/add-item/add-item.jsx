import React from 'react';
import moduleStyle from './add-item.module.scss';
const FA = require('react-fontawesome');

export default class AddItem extends React.Component{
  state = {
    active: true,
    valid: true,
    inputValue: ''
  }
  render(){
    const getTaskValue = (currentValue) =>{
      this.setState( ({inputValue})  =>{
        return{
          inputValue: currentValue
        }
      })
    }
    const onSubmit = (e) =>{
      e.preventDefault();
      this.props.addTask(this.state.inputValue);
    }
    const validationCheck = (value) =>{
      if (value) {
        this.setState(({ valid }) => {
          return { valid: true }
        })
      }
      else {
        this.setState(({ valid }) => {
          return { valid: false }
        })
      }
    }
    return(
      <div className={moduleStyle.popup_wrp}>
        <form className={`${moduleStyle.popup_wrp__popup} ${this.state.active ? moduleStyle.active : ''}`}
          onSubmit={onSubmit}
        >
          <div className={moduleStyle.popup_wrp__popup__input_wrp}>
            <input type="text" placeholder="Write task..." value={this.state.inputValue}
            onChange={ (elem) => {getTaskValue(elem.target.value); validationCheck(elem.target.value)} }/>
            <span className={moduleStyle.popup_wrp__popup__input_wrp__btm_line}></span>
          </div>
          <p className={`${moduleStyle.popup_wrp__validation_block} ${this.state.valid ? moduleStyle.hidden : ''}`}>{this.state.valid ? '' : 'Please type taskname'}</p>
          <button className={moduleStyle.popup_wrp__popup__add_btn} onClick={() => {validationCheck(this.state.inputValue)}}>Add task</button>
          <div className={moduleStyle.popup_wrp__popup__close_btn} onClick={ () => this.props.closeAddTaskPopup()}><FA name='times-circle' /></div>
        </form>
      </div>
    )
  }
}