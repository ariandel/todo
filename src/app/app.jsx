import React from 'react';
import Header from '../header';
import TodoList from '../todo-list';
import AddItem from '../add-item';
import './app.scss';
import moduleStyle from './app.module.scss';
const FA = require('react-fontawesome');

export default class App extends React.Component{
  
  state={
    filterStatus: 1,
    addTaskPopupState: false,
    
    data: [
      // {txt: 'task 1', id: 1, done: false, important: false},
      // {txt: 'task 2', id: 2, done: false, important: false},
      // {txt: 'task 3', id: 3, done: false, important: false},
      // {txt: 'task 4', id: 4, done: false, important: false},
    ],
    doneData: [
      
    ],
    deleteData: [
      
    ]
  }

  checkItemToggle = (id) =>{
    this.setState( ({data, doneData}) =>{
      for(let i = 0; i < data.length; i++){
        if(id === data[i].id){
          const currentElem = data[i];
          currentElem.done = !currentElem.done;
          let newArr = doneData;
          if(currentElem.done){
            newArr.push(currentElem);
            return{
              doneData: newArr
            }
          }
          else{
            const newDoneData = [
            ...[...newArr].splice(0, newArr.indexOf(currentElem)), 
            ...[...newArr].splice(newArr.indexOf(currentElem)+1, newArr.length)];
            return{
              doneData: newDoneData
            }
          }
        }
      }
    })
  }

  markImp = (id) =>{
    this.setState( ({data}) =>{
      for(let i = 0; i < data.length; i++){
        if(id === data[i].id){
          const currentElem = data[i];
          currentElem.important = !currentElem.important;
          return currentElem;
        }
      }
    })
  }
  
  addTask = (task) =>{
    this.setState( ({addTaskPopupState, data})  =>{
      if(task){
        let maxNumb = 0;
        if(data[0]){
          maxNumb = data[0].id;
        }
        for(let j = 0; j < data.length; j++){
          if(maxNumb < data[j].id){
            maxNumb = data[j].id;
          }
        }
        const id = maxNumb + 1;
        const taskObj = {txt: task, id: id, done: false, important: false};
        const newData = data;
        newData.push(taskObj);
        return{
          data: newData,
          addTaskPopupState: false
        }
      }
    });
  }

  deleteItem = (id) =>{
    this.setState( ({data, doneData, deleteData}) =>{
      for(let i = 0; i < data.length; i++){
        if(id === data[i].id){
          const firstArr = [...data].slice(0, i);
          const secondArr = [...data].slice(i+1);
          const newData = [...firstArr, ...secondArr];
          const newDeleteData = deleteData;
          newDeleteData.push(data[i]);
          if(data[i].done){
            for(let j = 0; j < doneData.length; j++){
              if(data[i].id === doneData[j].id){
                const newDoneData = [...[...doneData].slice(0,j), ...[...doneData].slice(j+1)];
                return{
                  data: newData,
                  doneData: newDoneData,
                  deleteData: newDeleteData
                }
              }
            }
          }
          return{
            data: newData,
            deleteData: newDeleteData
          }
        }
      }
    });
  }

  deletePermanently = (id) =>{
    this.setState( ({deleteData}) =>{
      for(let i = 0; i < deleteData.length; i++){
        if(id === deleteData[i].id){
          const firstArr = [...deleteData].slice(0, i);
          const secondArr = [...deleteData].slice(i+1);
          const newDeleteData = [...firstArr, ...secondArr];
          return{
            deleteData: newDeleteData
          }
        }
      }
    })
  }

  returnFromDelete = (id) =>{
    this.setState( ({data, doneData, deleteData}) =>{
      for(let i = 0; i < deleteData.length; i++){
        if(id === deleteData[i].id){
          const newDeleteData = [...[...deleteData].slice(0, deleteData.indexOf(deleteData[i])), ...[...deleteData].slice(deleteData.indexOf(deleteData[i])+1)];
          const newArrData = data;
          let maxNumb = 0;
          if(data[0]){
            maxNumb = data[0].id;
          }
          for(let j = 0; j < data.length; j++){
            if(maxNumb < data[j].id){
              maxNumb = data[j].id;
            }
          }
          newArrData.push(deleteData[i]);
          newArrData[newArrData.length - 1].id = maxNumb + 1;
          if (deleteData[i].done) {
            const newDoneData = doneData;
            newDoneData.push(deleteData[i]);
            return {
              data: newArrData,
              doneData: newDoneData,
              deleteData: newDeleteData
            }
          }
          return{
            data: newArrData,
            deleteData: newDeleteData
          }
        }
      }
    })
  }

  filterAll = () =>{
    this.setState( ({filterStatus}) =>{
      return{
        filterStatus: 1
      }
    })
  }

  filterDone = () =>{
    this.setState( ({filterStatus}) =>{
      return{
        filterStatus: 2
      }
    })
  }

  filterDeleted = () =>{
    this.setState( ({filterStatus}) =>{
      return{
        filterStatus: 3
      }
    })
  }

  showAddTaskPopup = () =>{
    this.setState( ({addTaskPopupState}) =>{
      const newState = !addTaskPopupState;
      return {addTaskPopupState: newState}
    })
  }

  closeAddTaskPopup = () =>{
    this.setState( ({addTaskPopupState}) =>{
      return {addTaskPopupState: false};
    })
  }

  render(){
    const {data, doneData, deleteData, filterStatus, addTaskPopupState} = this.state;
    const AddTaskPopup = () =>{
      if(addTaskPopupState){
        return <AddItem addTask={this.addTask} closeAddTaskPopup={this.closeAddTaskPopup} />
      }
      else{
        return <></>
      }
    };

    return (
      <div className={moduleStyle.main_wrapper}>
        <Header 
          filterAll={this.filterAll} filterDone={this.filterDone} 
          filterDeleted={this.filterDeleted} filterStatus={filterStatus}
           />
        <button className={moduleStyle.addTask_btn} onClick={this.showAddTaskPopup}>
          <span className={moduleStyle.addTask_btn__plus}></span>
          <span className={moduleStyle.addTask_btn__txt}>Add task</span>
        </button>
        <AddTaskPopup />
        <TodoList 
        data={data} doneData={doneData} deleteData={deleteData} filterStatus={filterStatus} 
        checkItemToggle={this.checkItemToggle} markImp={this.markImp} deleteItem={this.deleteItem}
        returnFromDelete={this.returnFromDelete} deletePermanently={this.deletePermanently}
         />
         <div className={moduleStyle.todo_bg_txt}>ToDo</div>
      </div>
    )
  }
}